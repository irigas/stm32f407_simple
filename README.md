# Simple programs:

##########################
    usb echo
##########################
Description: Receives a string from usb and returns the same string
how to test:
        make clean && make && make burn
        on one terminal:
                cat /dev/ttyACM0
        on a second terminal:
                echo test > /dev/ttyACM0

